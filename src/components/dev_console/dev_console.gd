tool
extends PopupPanel


onready var command_scroll_container = $Container/CommandScrollContainer
onready var command_container = $Container/CommandScrollContainer/ComandContainer
onready var command_edit = $Container/CommandEdit

onready var command_entry_template = load('res://components/dev_console/dev_console_entry.tscn')

onready var is_enabled = OS.has_feature('editor')


func _ready():
	if Engine.editor_hint:
		command_container.add_child(command_entry_template.instance())
		command_container.add_child(command_entry_template.instance())
	else:
		command_edit.text = ''
		command_edit.connect('text_entered', self, '__on_text_entered')
		connect('popup_hide', self, '__fade_last_entry')


func _input(_event):
	if is_enabled && Input.is_action_just_pressed("debug_dev_console"):
		call_deferred('popup')


func __on_text_entered(raw_command: String):
	var split_command = raw_command.split(' ')
	var command = '__cmd_%s'%split_command[0]
	split_command.remove(0)

	var new_entry = command_entry_template.instance()

	var success = true
	if has_method(command):
		var result = call(command, split_command)
		if result:
			if typeof(result) == TYPE_STRING:
				new_entry.set_data_result(raw_command, result)
			elif result.has('is_error'):
				new_entry.set_data_error(raw_command, result.get('message', 'Error'))
				success = false
			else:
				new_entry.set_data_result(raw_command, result.get('message', 'ok...'))
		else:
			new_entry.set_data_ok(raw_command)
	else:
		new_entry.set_data_error(raw_command, 'Command not defined!')
		success = false

	if success:
		command_edit.text = ''

	__fade_last_entry()
	command_container.add_child(new_entry)
	command_container.move_child(new_entry, 0)


func __fade_last_entry():
	if command_container.get_child_count() > 0:
		command_container.get_child(0).modulate = Color(1, 1, 1, 0.6)


func __cmd_clear(_args):
	for child in command_container.get_children():
		child.queue_free()


func __cmd_help(_args):
	var command_names = []
	for method in get_method_list():
		if method.name.begins_with('__cmd_'):
			command_names.append(method.name.trim_prefix('__cmd_'))
	return 'available commands:\n ' + PoolStringArray(command_names).join('\n ')


# Add custom commands below:


func __cmd_echo(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <text>' }

	return args[0]


func __cmd_reset_level(args):
	if args.size() == 2:
		STATE.reset_level(1, args[0], int(args[1]))
	elif args.size() == 0:
		STATE.reset_level(1, ID.GRASS, 10)
	else:
		return { is_error=true, message='invalid arguments! expected: <environment> <width>' }

	SCENE.goto_scene('world')


func __cmd_cash(_args):
	STATE.set_budget(1_000_000_000)
	EVENTS.emit_signal('module_placed') # to refresh stats


func __cmd_destroy(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <disaster>' }
	
	EVENTS.emit_signal('destroy_%s'%args[0])


func __cmd_module(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <module-id>' }
	
	var arg_id = args[0].to_upper()
	var resolved_id = ID.get(arg_id)

	if resolved_id == null:
		return { is_error=true, message='invalid arguments! %s is not a valid id'%arg_id }

	EVENTS.emit_signal('module_selected',resolved_id)
