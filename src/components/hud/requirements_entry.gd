extends HBoxContainer


var PROGRESS_SPEED = 1.0

var id
var max_value
var type

var current_value = 0


func _ready():
	EVENTS.connect('module_placed', self, '_on_update_value')


func set_data(new_id: String, value):
	match new_id:
		ID.REQUIRED_MODULE:
			$ProgressBar/HBox/Text.set_text('%s %s modules'%[STATE.format_number(value.amount), DATA.CARDS[value.type].name])
		ID.REQUIRED_CATEGORY:
			$ProgressBar/HBox/Text.set_text('%s modules of the category %s'%[STATE.format_number(value.amount), DATA.CARD_CATEGORIES[value.type].name])
		ID.SPACE:
			$ProgressBar/HBox/Text.set_text('Space for %s inhabitants'%[STATE.format_number(value)])
		ID.HEIGHT:
			$ProgressBar/HBox/Text.set_text('%s stories high'%[STATE.format_number(value)])
		ID.ENTERTAINMENT_FACTOR:
			$ProgressBar/HBox/Text.set_text('An entertainment score of %s'%[STATE.format_number(value)])
		ID.WORK:
			$ProgressBar/HBox/Text.set_text('Space for %s workers'%[STATE.format_number(value)])
	
	if value is float or value is int:
		$ProgressBar.set_max(value)
		max_value = value
		$ProgressBar/HBox/Amount.set_text('0 / %s'%[STATE.format_number(value)])
	else:
		$ProgressBar.set_max(value.amount)
		max_value = value.amount
		type = value.type
		$ProgressBar/HBox/Amount.set_text('0 / %s'%[STATE.format_number(value.amount)])
	id = new_id
	PROGRESS_SPEED = max(max_value / 4.0, 1.0)


func _on_update_value():
	var new_value = 0
	match id:
		ID.REQUIRED_MODULE:
			new_value = STATE.get_single_module_count(type)
		ID.REQUIRED_CATEGORY:
			new_value = STATE.get_module_category_count(type)
		ID.SPACE:
			new_value = STATE.get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
		ID.HEIGHT:
			new_value = STATE.get_tower_height()
		ID.ENTERTAINMENT_FACTOR:
			new_value = STATE.get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR)
		ID.WORK:
			new_value = STATE.get_module_category_value(ID.OFFICE_SPACE, ID.WORK) + STATE.get_module_category_value(ID.ENTERTAINMENT, ID.WORK)
	
	current_value = new_value
	$ProgressBar/HBox/Amount.set_text('%s / %s'%[STATE.format_number(new_value), STATE.format_number(max_value)])
	if new_value >= max_value:
		$CheckBox.set_pressed(true)
	else:
		$CheckBox.set_pressed(false)


func _process(delta):
	var bar_value = $ProgressBar.get_value()
	if bar_value < current_value:
		$ProgressBar.set_value(bar_value + PROGRESS_SPEED * delta)
		if $ProgressBar.get_value() > current_value:
			$ProgressBar.set_value(current_value)
	elif bar_value > current_value:
		$ProgressBar.set_value(bar_value - PROGRESS_SPEED * delta)
		if $ProgressBar.get_value() < current_value:
			$ProgressBar.set_value(current_value)
