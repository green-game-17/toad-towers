extends CanvasLayer


const READY_MOVEMENT_SPEED = 500


var delete_active = false
var ready_to_submit = false
var downwards = false


# Called when the node enters the scene tree for the first time.
func _ready():
	$ButtonArray/Panel/Margin/VBox/DeleteButton.connect('pressed', self, '_on_delete_triggered')
	$ButtonArray/Panel/Margin/VBox/ContractButton.connect('pressed', self, '_on_contract_triggered')
	EVENTS.connect('disaster_struck', self, '_on_disaster_struck')
	EVENTS.connect('last_disaster_success', self, '_on_last_disaster_success')
#	EVENTS.connect('ready_to_submit', self, '_on_ready_to_submit') # Something needs to be done to get the players attention, but idk


func _on_delete_triggered():
	delete_active = !delete_active
	EVENTS.emit_signal('delete_triggered', delete_active)
	STATE.set_delete_active(delete_active)
	if delete_active:
		$DeleteInfo.show()
	else:
		$DeleteInfo.hide()


func _on_contract_triggered():
	$Contract.show()


func _on_disaster_struck(_id: String):
	$ButtonArray.hide()


func _on_last_disaster_success():
	$ButtonArray.hide()
