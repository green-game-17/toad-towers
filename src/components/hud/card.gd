extends PanelContainer


signal gets_selected(index)
signal is_hovered(index, is_hovered)


const MIN_SCALE = Vector2(0.35, 0.35)
const MAX_SCALE = Vector2(1, 1)
const SCALE_SPEED = Vector2(3.5, 3.5)
const FADE_SPEED = 5


export var has_gui_input: bool = true
export var is_special_card: bool = false

var entry_scene = load('res://components/hud/card_stat_entry.tscn')
var stylebox = load('res://assets/styles/card_separator.tres')

var id
var data
var stack_position = 0

var is_mouse_inside = false
var delete_active = false

var show_self = false
var delete_self = false


func _ready():
	EVENTS.connect('delete_triggered', self, '_on_delete_triggered')
	
	$ClickArea.connect('mouse_entered', self, '_on_mouse_entered')
	$ClickArea.connect('mouse_exited', self, '_on_mouse_exited')
	$ClickArea.connect('gui_input', self, '_on_gui_input')
	
	show_self = true
	
	delete_active = STATE.get_delete_active()
	
	if !has_gui_input:
		$ClickArea.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)


func set_id(new_id: String):
	id = new_id
	data = DATA.CARDS[id]
	
	add_stylebox_override('panel', get_stylebox('panel').duplicate())
	$Margin/Panel/VBox/Category.add_stylebox_override('normal', $Margin/Panel/VBox/Category.get_stylebox('normal').duplicate())
	
	var category_data = DATA.CARD_CATEGORIES[data.category]
	$Margin/Panel/VBox/Category.set_text(category_data.name)
	get_stylebox('panel').set_bg_color(Color(1, 1, 1, 1).linear_interpolate(category_data.color, 0.1))
	$Margin/Panel/VBox/Category.get_stylebox('normal').set_bg_color(category_data.color)
	$Margin/Panel/VBox/Title.set_text(data.name)
	$Margin/Panel/VBox/Image.set_texture(data.image)
	$Margin/Panel/VBox/Stats/VBox/Description.set_text(data.description)
	$Margin/Panel/VBox/Costs/HBox/Amount.set_text(STATE.format_number(data.costs))
	
	for child in $Margin/Panel/VBox/Stats/VBox/Control/Panel/VBox/Entries.get_children():
		child.queue_free()
		yield(child, 'tree_exited')
	
	for key in data.size:
		__add_entry(key, data.size[key])
	
	for key in data.stats:
		__add_entry(key, data.stats[key])
	
	if 'placement_requirements' in data:
		if ID.MAX_HEIGHT in data.placement_requirements:
			__add_entry(ID.MAX_HEIGHT, data.placement_requirements[ID.MAX_HEIGHT])


func __add_entry(key: String, value: float):
	if $Margin/Panel/VBox/Stats/VBox/Control/Panel/VBox/Entries.get_child_count() > 0:
		var new_separator = HSeparator.new()
		new_separator.add_stylebox_override('separator', stylebox)
		new_separator.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
		$Margin/Panel/VBox/Stats/VBox/Control/Panel/VBox/Entries.add_child(new_separator)
	var new_entry = entry_scene.instance()
	new_entry.set_data(key, value)
	$Margin/Panel/VBox/Stats/VBox/Control/Panel/VBox/Entries.add_child(new_entry)


func set_stack_position(new_position: int):
	stack_position = new_position
	show_self = false


func remove_self():
	$ClickArea.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
	delete_self = true
	stack_position = 4


func _on_mouse_entered():
	if has_gui_input:
		is_mouse_inside = true
		emit_signal('is_hovered', stack_position, true)


func _on_mouse_exited():
	if has_gui_input:
		is_mouse_inside = false
		emit_signal('is_hovered', stack_position, false)


func _on_gui_input(event: InputEvent):
	if event is InputEventMouseButton and !event.is_pressed() and is_mouse_inside and (stack_position == 3 or is_special_card or delete_active):
		emit_signal('gets_selected', stack_position)
		EVENTS.emit_signal('module_selected', id)


func _on_delete_triggered(is_active: bool):
	delete_active = is_active


func _process(delta):
	if is_special_card:
		return
	
	if is_mouse_inside and get_scale() < MAX_SCALE:
		set_scale(get_scale() + SCALE_SPEED * delta)
		if get_scale() > MAX_SCALE:
			set_scale(MAX_SCALE)
	elif !is_mouse_inside and get_scale() > MIN_SCALE:
		set_scale(get_scale() - SCALE_SPEED * delta)
		if get_scale() < MIN_SCALE:
			set_scale(MIN_SCALE)
	
	var delete_modulate = $Delete.get_modulate()
	if is_mouse_inside and delete_active and delete_modulate.a < 1.0:
		delete_modulate.a += FADE_SPEED * delta
		$Delete.set_modulate(delete_modulate)
	elif !is_mouse_inside and delete_modulate.a > 0.0:
		delete_modulate.a -= FADE_SPEED * delta
		$Delete.set_modulate(delete_modulate)
	
	if show_self:
		var own_modulate = get_modulate()
		own_modulate.a += (FADE_SPEED / 1.5) * delta
		if own_modulate.a >= 0.8:
			show_self = false
			own_modulate.a = 0.8
		set_modulate(own_modulate)
	
	if delete_self:
		var own_modulate = get_modulate()
		own_modulate.a -= (FADE_SPEED / 1.5) * delta
		if own_modulate.a <= 0.0:
			queue_free()
		else:
			set_modulate(own_modulate)
