extends Control


var contract_data = {}
var done_disasters = []


func _ready():
	EVENTS.connect('module_placed', self, '_on_refresh')
	$Button.connect('pressed', self, '_on_submit')

	$Button.hide()

	contract_data = DATA.CONTRACTS[STATE.get_selected_level()]


func _on_refresh():
	for key in contract_data.disasters:
		var value = contract_data.disasters[key]

		if key is int and STATE.get_tower_height() >= key and not key in done_disasters:
			done_disasters.append(key)
			if not __check_for_disaster(value.type, value.strength):
				hide()
				__fail(value.type)
				return

	for key in contract_data.min_requirements:
		var value = contract_data.min_requirements[key]
		var is_valid = true
		match key:
			ID.HEIGHT:
				is_valid = STATE.get_tower_height() >= value
			ID.SPACE:
				is_valid = STATE.get_module_category_value(ID.LIVING_SPACE, ID.SPACE) >= value
			ID.ENTERTAINMENT_FACTOR:
				is_valid = STATE.get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR) >= value
			ID.WORK:
				is_valid = STATE.get_module_category_value(ID.OFFICE_SPACE, ID.WORK) + STATE.get_module_category_value(ID.ENTERTAINMENT, ID.WORK) >= value
			ID.REQUIRED_MODULE:
				is_valid = STATE.get_single_module_count(value.type) >= value.amount
			ID.REQUIRED_CATEGORY:
				is_valid = STATE.get_module_category_count(value.type) >= value.amount
		if not is_valid:
			print('[SubmitButton] _on_refresh: requirement not met yet: %s'%key)
			return

	# all good!
	$Label.hide()
	$Button.show()
	EVENTS.emit_signal('ready_to_submit')


func _on_submit():
	hide()

	var disaster_value = contract_data.disasters.end

	var is_safe = __check_for_disaster(disaster_value.type, disaster_value.strength)
	if is_safe:
		__win()
	else:
		__fail(disaster_value.type)


func __fail(id):
	EVENTS.emit_signal('disaster_struck', id)
	yield(get_tree().create_timer(10), "timeout")
	EVENTS.emit_signal('level_failed', id)


func __win():
	EVENTS.emit_signal('last_disaster_success')
	yield(get_tree().create_timer(2), "timeout")
	EVENTS.emit_signal('level_success')


func __check_for_disaster(id: String, value: int):
	var disaster_signal = null 
	var reached_value = 0
	var stability_check = false
	var extra_fail = false
	var stat = null

	match id:
		ID.EARTHQUAKE:
			stat = ID.EARTHQUAKE_RESISTANCE
			stability_check = true
			disaster_signal = 'destroy_quake'
		ID.SNAKENADO:
			stat = ID.SNAKENADO_RESISTANCE
			stability_check = true
			disaster_signal = 'destroy_snakenado'
		ID.TORNADO:
			stat = ID.WIND_RESISTANCE
			stability_check = true
			disaster_signal = 'destroy_wind'
		ID.COLDNESS:
			stat = ID.TEMPERATURE_RESISTANCE
			disaster_signal = 'destroy_temperature'
		ID.INFERNO:
			stat = ID.FIRE_RESISTANCE
			disaster_signal = 'destroy_fire'
		ID.FLOOD:
			stat = ID.WATER_RESISTANCE
			disaster_signal = 'destroy_water'
#			var water_height = int(value/100)
#			if not STATE.calc_drowning_is_safe(water_height):
#				print('[SubmitButton] __check_for_disaster: failed against water - living space below height %d'%[water_height])
#				extra_fail = true
		_:
			print('[SubmitButton] __check_for_disaster: failed against %s - unknown'%[id])
			return false
		
	if stability_check:
		reached_value += STATE.calc_stability_score()
	reached_value += STATE.get_module_category_value(ID.DISASTER_PREVENTION, stat)

	if reached_value < value or extra_fail:
		EVENTS.emit_signal(disaster_signal)
		print('[SubmitButton] __check_for_disaster: failed against %s - reached %d / %d'%[id, reached_value, value])
		return false
	else:
		print('[SubmitButton] __check_for_disaster: was safe against %s - reached %d / %d'%[id, reached_value, value])
		return true
