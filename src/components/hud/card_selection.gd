extends Control


const OK_COLOR = Color(0.36, 0.82, 0.31, 1)
const WARNING_COLOR = Color(0.82, 0.8, 0.31, 1)
const BAD_COLOR = Color(0.82, 0.31, 0.31, 1)

const MIN_HEIGHT = 392
const MAX_HEIGHT = 900
const STACK_MOVEMENT_DIST_4 = 220
const STACK_MOVEMENT_DIST_9 = 270
const STACK_MOVEMENT_SPEED = 1500
const STACKS_MOVING = 7

var selected_stack = null
var contract_data = null
var special_cards_played = []
var special_card_visible = false
var delete_active = false

var card_hovered = null
var available_height = MAX_HEIGHT - MIN_HEIGHT
var part_height = 0


func _ready():
	randomize()
	
	EVENTS.connect('module_placed', self, '_on_module_placed')
	EVENTS.connect('module_deselected', self, '_on_module_deselected')
	EVENTS.connect('delete_triggered', self, '_on_delete_triggered')
	
	for i in $Stacks.get_child_count():
		$Stacks.get_child(i).connect('card_selected', self, '_on_card_select', [i])
		$Stacks.get_child(i).connect('card_hovered', self, '_on_card_hovered', [i])
	
	contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	part_height = (available_height / (contract_data.stacks.size() + 1))
	for i in contract_data.stacks.size():
		var child = $Stacks.get_child(i)
		child.set_category(contract_data.stacks[i])
		child.set_position(Vector2(0, part_height * (i + 1) + MIN_HEIGHT))
		child.show()


func _on_card_select(index: int):
	if delete_active:
		if STATE.has_enough_budget_to_delete():
			$Stacks.get_child(index).remove_card()
			STATE.decrease_budget_by_delete()
			STATE.inc_discarded_cards()
			EVENTS.emit_signal('module_placed')
	else:
		selected_stack = index


func _on_card_hovered(is_hovered: bool, index: int):
	if is_hovered:
		card_hovered = index
	else:
		card_hovered = null


func _on_module_placed():
	if selected_stack == null and not special_card_visible:
		return
	if not special_card_visible:
		$Stacks.get_child(selected_stack).remove_card(3)
		selected_stack = null
	else:
		special_card_visible = false
		$Stacks.show()
		$SpecialCard.hide()
		EVENTS.emit_signal('special_card_visible', false)
	
	var tower_height = STATE.get_tower_height()
	for height in range(tower_height - 3, tower_height + 1):
		if not height in special_cards_played:
			if height in contract_data.special_cards:
				$SpecialCard.set_id(contract_data.special_cards[height])
				special_cards_played.append(height)
				special_card_visible = true
				
				$Stacks.hide()
				$SpecialCard.show()
				EVENTS.emit_signal('special_card_visible', true)
				break


func _on_module_deselected():
	selected_stack = null


func _on_delete_triggered(is_active: bool):
	delete_active = is_active
	if !delete_active:
		card_hovered = null


func _process(delta):
	if card_hovered != null:
		var stack_count = contract_data.stacks.size()
		var movement_dist = 0
		if stack_count >= 4:
			movement_dist = STACK_MOVEMENT_DIST_4 + ((STACK_MOVEMENT_DIST_9 - STACK_MOVEMENT_DIST_4) / 5) * (stack_count - 4)
		for i in stack_count:
			var stack = $Stacks.get_child(i)
			var orig_y = part_height * (i + 1) + MIN_HEIGHT
			if i == card_hovered:
				__move_stack(stack, orig_y, delta)
			elif i < card_hovered:
				var dest_y = orig_y - movement_dist + (movement_dist / STACKS_MOVING) * clamp((card_hovered - 1) - i, 0, STACKS_MOVING)
				__move_stack(stack, dest_y, delta)
			elif i > card_hovered:
				var dest_y = orig_y + movement_dist - (movement_dist / STACKS_MOVING) * clamp(i - (card_hovered + 1), 0, STACKS_MOVING)
				__move_stack(stack, dest_y, delta)
	else:
		for i in contract_data.stacks.size():
			var stack = $Stacks.get_child(i)
			var orig_y = part_height * (i + 1) + MIN_HEIGHT
			__move_stack(stack, orig_y, delta)


func __move_stack(stack: Control, dest: int, delta: float):
	var stack_pos = stack.get_position()
	if stack_pos.y > dest:
		stack.set_position(Vector2(stack_pos.x, stack_pos.y - STACK_MOVEMENT_SPEED * delta))
		var new_pos = stack.get_position()
		if new_pos.y < dest:
			stack.set_position(Vector2(new_pos.x, dest))
	elif stack_pos.y < dest:
		stack.set_position(Vector2(stack_pos.x, stack_pos.y + STACK_MOVEMENT_SPEED * delta))
		var new_pos = stack.get_position()
		if new_pos.y > dest:
			stack.set_position(Vector2(new_pos.x, dest))
