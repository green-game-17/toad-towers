extends Control


func _ready():
	EVENTS.connect('module_placed', self, '_update_data')


func _update_data():
	$Panel/VBox/Earthquake/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.EARTHQUAKE,
		STATE.calc_stability_score() + STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.EARTHQUAKE_RESISTANCE)
	))
	$Panel/VBox/Snakenado/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.SNAKENADO,
		STATE.calc_stability_score() + STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.SNAKENADO_RESISTANCE)
	))
	$Panel/VBox/Tornado/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.TORNADO,
		STATE.calc_stability_score() + STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.WIND_RESISTANCE)
	))
	$Panel/VBox/Coldness/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.COLDNESS,
		STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.TEMPERATURE_RESISTANCE)
	))
	$Panel/VBox/Flood/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.FLOOD,
		STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.WATER_RESISTANCE)
	))
	$Panel/VBox/Inferno/Margin/VBox/Amount.set_text(STATE.format_disaster_value(
		ID.INFERNO,
		STATE.get_module_category_value(ID.DISASTER_PREVENTION, ID.FIRE_RESISTANCE)
	))
