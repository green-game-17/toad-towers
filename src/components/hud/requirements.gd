extends Control


const OK_COLOR = Color(0.36, 0.82, 0.31, 1)
const WARNING_COLOR = Color(0.82, 0.8, 0.31, 1)
const BAD_COLOR = Color(0.82, 0.31, 0.31, 1)
const INFO_COLOR = Color(0.31, 0.82, 0.65, 1)
const ICON_SHRINK_SPEED = 600
const ICON_MOVE_SPEED = 600

var icons = {
	'ok': load('res://assets/icons/safe.png'),
	'warning': load('res://assets/icons/warning.png'),
	'failed': load('res://assets/icons/failed.png'),
	'info': load('res://assets/icons/info.png'),
	ID.EARTHQUAKE: load('res://assets/icons/earthquake.png'),
	ID.TORNADO: load('res://assets/icons/tornado.png'),
	ID.INFERNO: load('res://assets/icons/inferno.png'),
	ID.SNAKENADO: load('res://assets/icons/snakenado.png'),
	ID.FLOOD: load('res://assets/icons/flood.png'),
	ID.COLDNESS: load('res://assets/icons/coldness.png')
}
var entry_scene = load('res://components/hud/requirements_entry.tscn')
var contract_data

var special_card_visible = false
var current_icon_type = 'ok'
var shrink_icon = false
var is_ready_to_submit = false

onready var old_icon_pos = $HBox/Info/VBox/Control/Image.get_position()


# Called when the node enters the scene tree for the first time.
func _ready():
	contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	for key in contract_data.min_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.min_requirements[key])
		$HBox/MinReq/VBox/Margin/VBox.add_child(new_entry)
	for key in contract_data.bonus_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.bonus_requirements[key])
		$HBox/BonusReq/VBox/Margin/VBox.add_child(new_entry)
	$Budget/Margin/HBox/Value.set_text(STATE.format_number(contract_data.budget))
	
	$IconTimer.connect('timeout', self, '_on_icontimer_timeout')
	
	EVENTS.connect('module_placed', self, '_on_update_budget')
	EVENTS.connect('ready_to_submit', self, '_on_ready_to_submit')
	EVENTS.connect('special_card_visible', self, '_on_special_card_visible')
	EVENTS.connect('disaster_struck', self, '_on_disaster_struck')


func _on_update_budget():
	$Budget/Margin/HBox/Value.set_text(STATE.format_number(STATE.get_budget()))
	
	if special_card_visible:
		return
	
	var tower_height = STATE.get_tower_height()
	for i in range(1, 4):
		if tower_height + i in contract_data.disasters:
			var disaster = contract_data.disasters[tower_height + i]
			$HBox/Info/VBox/Text.set_text('Next %s with a strength of %s in %s stories'%[
				DATA.DISASTERS[disaster.type].name,
				STATE.format_disaster_value(disaster.type, disaster.strength),
				i
			])
			$HBox/Info.get_stylebox('panel').set_bg_color(WARNING_COLOR)
			__set_new_icon('warning')
			return
	
	$HBox/Info/VBox/Text.set_text('Everything is fine')
	$HBox/Info.get_stylebox('panel').set_bg_color(OK_COLOR)
	__set_new_icon('ok')


func _on_special_card_visible(is_visible: bool):
	special_card_visible = is_visible
	if special_card_visible:
		$HBox/Info/VBox/Text.set_text('The client wants you to use this card')
		$HBox/Info.get_stylebox('panel').set_bg_color(INFO_COLOR)
		__set_new_icon('info')


func _on_ready_to_submit():
	$HBox/Info/VBox/Text.set_text('Brace yourself, %s with a strength of %s is coming'%[
		DATA.DISASTERS[contract_data.disasters.end.type].name,
		STATE.format_disaster_value(contract_data.disasters.end.type, contract_data.disasters.end.strength)
		])
	$HBox/Info.get_stylebox('panel').set_bg_color(WARNING_COLOR)
	__set_new_icon('warning')
	is_ready_to_submit = true


func _on_disaster_struck(disaster_id: String):
	$HBox/Info/VBox/Text.set_text('%s struck your building'%[DATA.DISASTERS[disaster_id].name])
	$HBox/Info.get_stylebox('panel').set_bg_color(BAD_COLOR)
	__set_new_icon(disaster_id)


func __set_new_icon(icon_type: String):
	var image = $HBox/Info/VBox/Control/Image
	image.set_texture(icons[icon_type])
	if icon_type != current_icon_type and (!is_ready_to_submit or (is_ready_to_submit and icon_type != 'ok')):
		var old_pos = image.get_position()
		var old_size = image.get_size()
		old_pos.y = old_icon_pos.y + 150
		old_size.y = 200
		image.set_position(old_pos)
		image.set_size(old_size)
		current_icon_type = icon_type
		shrink_icon = false
		$IconTimer.start()


func _on_icontimer_timeout():
	shrink_icon = true


func _process(delta):
	if shrink_icon:
		var image = $HBox/Info/VBox/Control/Image
		var old_pos = image.get_position()
		var old_size = image.get_size()
		
		if old_pos.y > old_icon_pos.y:
			old_pos.y -= ICON_MOVE_SPEED * delta
			image.set_position(old_pos)
			if old_pos.y < old_icon_pos.y:
				old_pos.y = old_icon_pos.y
				image.set_position(old_pos)
		if old_size.y > 50:
			old_size.y -= ICON_SHRINK_SPEED * delta
			image.set_size(old_size)
			if old_size.y < 50:
				old_size.y = 50
				image.set_size(old_size)
		if old_pos.y == old_icon_pos.y and old_size.y == 50:
			shrink_icon = false
