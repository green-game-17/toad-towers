extends Control


signal card_selected()
signal card_hovered(is_hovered)


const CARD_SPACE = 50
const CARD_MOVEMENT_DIST = 280
const CARD_MOVEMENT_SPEED = 1500

var card_scene = load('res://components/hud/card.tscn')
var category
var card_ids = []
var current_ids = []
var card_hovered = null


func _ready():
	randomize()
	
	EVENTS.connect('delete_triggered', self, '_on_delete_triggered')


func set_category(id: String):
	category = id
	card_ids = DATA.get_card_ids_by_category(category)
	if get_child_count() > 0:
		for child in get_children():
			child.queue_free()
		current_ids = []
	for i in 4:
		add_card(true)


func add_card(on_start: bool, deleted_index = null):
	var id
	var contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	if 'preferred_cards' in contract_data:
		var relevant_ids = []
		for card_id in card_ids:
			if card_id in contract_data.preferred_cards:
				relevant_ids.append(card_id)
				continue
			for stat in DATA.CARDS[card_id].stats:
				if stat in contract_data.preferred_cards:
					relevant_ids.append(card_id)
					break
		if relevant_ids.size() > 0 and randf() <= 0.7:
			id = relevant_ids[randi() % relevant_ids.size()]
		else:
			id = card_ids[randi() % card_ids.size()]
	else:
		id = card_ids[randi() % card_ids.size()]
	
	for index in get_child_count():
		var child = get_child(index)
		if on_start:
			child.set_position(Vector2((index + 1) * CARD_SPACE, -282.5))
		elif deleted_index != null and index > deleted_index:
			break
		child.set_stack_position(index + 1)
		if index + 1 == 1:
			child.set_modulate(Color(0.7, 0.7, 0.7, 0.85))
		elif index + 1 == 2:
			child.set_modulate(Color(0.7, 0.7, 0.7, 0.9))
		elif index + 1 == 3:
			child.set_modulate(Color(1, 1, 1, 1))
	
	var new_child = card_scene.instance()
	new_child.set_id(id)
	current_ids.push_front(id)
	new_child.set_scale(new_child.MIN_SCALE)
	new_child.set_modulate(Color(0.7, 0.7, 0.7, 0.0))
	if on_start:
		new_child.set_position(Vector2(0, -282.5))
	else:
		new_child.set_position(Vector2(-CARD_SPACE, -282.5))
	new_child.connect('gets_selected', self, '_on_card_selected')
	new_child.connect('is_hovered', self, '_on_card_hovered')
	
	add_child(new_child)
	move_child(new_child, 0)


func _on_card_selected(_stack_index: int):
	emit_signal('card_selected')


func _on_card_hovered(stack_index: int, is_hovered: bool):
	if is_hovered:
		card_hovered = stack_index
	else:
		card_hovered = null
	emit_signal('card_hovered', is_hovered)


func remove_card(index: int = card_hovered):
	var index_copy = index
	for child_index in get_child_count():
		if !get_children()[child_index].delete_self:
			index_copy -= 1
		if index_copy == -1:
			get_children()[child_index].remove_self()
			break
	current_ids.remove(index)
	add_card(false, index)


func _on_delete_triggered(is_active: bool):
	if !is_active:
		card_hovered = null


func _process(delta):
	if card_hovered != null:
		for i in get_child_count():
			var child = get_child(i)
			var child_pos = child.get_position()
			if i > card_hovered and child_pos.x < min(i, 3) * CARD_SPACE + CARD_MOVEMENT_DIST:
				child.set_position(Vector2(child_pos.x + CARD_MOVEMENT_SPEED * delta, child_pos.y))
				var new_pos = child.get_position()
				if new_pos.x > min(i, 3) * CARD_SPACE + CARD_MOVEMENT_DIST:
					child.set_position(Vector2(i * CARD_SPACE + CARD_MOVEMENT_DIST, new_pos.y))
			elif i <= card_hovered and child_pos.x > min(i, 3) * CARD_SPACE:
				child.set_position(Vector2(child_pos.x - CARD_MOVEMENT_SPEED * delta, child_pos.y))
				var new_pos = child.get_position()
				if new_pos.x < min(i, 3) * CARD_SPACE:
					child.set_position(Vector2(i * CARD_SPACE, new_pos.y))
	else:
		for i in get_child_count():
			var child = get_child(i)
			var child_pos = child.get_position()
			if child_pos.x > i * CARD_SPACE:
				child.set_position(Vector2(child_pos.x - CARD_MOVEMENT_SPEED * delta, child_pos.y))
				var new_pos = child.get_position()
				if new_pos.x < i * CARD_SPACE:
					child.set_position(Vector2(i * CARD_SPACE, new_pos.y))
			elif child_pos.x < i * CARD_SPACE:
				child.set_position(Vector2(child_pos.x + (CARD_MOVEMENT_SPEED / 10) * delta, child_pos.y))
				var new_pos = child.get_position()
				if new_pos.x > i * CARD_SPACE:
					child.set_position(Vector2(i * CARD_SPACE, new_pos.y))
