extends MarginContainer


var key
var value = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func set_data(new_key: String, new_value: float):
	key = new_key
	value = new_value
	$HBox/Name.set_text(DATA.CARD_STATS[key].name)
	$HBox/Value.set_text(STATE.format_disaster_value(key, value))
