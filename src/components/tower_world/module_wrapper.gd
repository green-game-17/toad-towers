extends Spatial


var physics_body # can't use onready as that runs after set_module is called
var module_size
var tower_position = Vector2.ZERO

onready var rng = RandomNumberGenerator.new()

onready var fire_scenes = [
	load('res://components/disaster/fire1.tscn'),
	load('res://components/disaster/fire2.tscn'),
	load('res://components/disaster/fire3.tscn')
]


func _ready():
	rng.randomize()


func set_module(module_node, size, target_translation):
	physics_body = $PhysicsBody
	
	var collider = physics_body.get_child(0)
	if size.y > 1:
		collider.translation.y = (float(size.y) / 2) * DATA.GRID_SIZE
	var shape = collider.shape.duplicate()
	collider.shape = shape
	shape.extents.x = float(size.x * DATA.GRID_SIZE) / 2
	shape.extents.y = float(size.y * DATA.GRID_SIZE) / 2

	physics_body.add_child(module_node)
	module_node.translation = Vector3.ZERO
	var module_material = module_node.get_child(0).get_active_material(0).duplicate()
	module_node.get_child(0).set_surface_material(0, module_material)

	set_translation(target_translation)

	module_size = size


func set_tower_position(x, y):
	tower_position = Vector2(x, y)


func destroy_quake():
	physics_body.mode = RigidBody.MODE_RIGID
	
	var durr = 5 * 60
	for i in range(1, durr):
		physics_body.linear_velocity.x = rng.randf() * 8 - 4
		var y_velocity = -0.7
		if STATE.get_tower_height() < 5:
			y_velocity = -2
		physics_body.linear_velocity.y = lerp(0, y_velocity, durr/(durr-i))
		physics_body.linear_velocity.z = rng.randf() * 8 - 4
		yield(get_tree().create_timer(1/20), "timeout")

	physics_body.linear_velocity.x = 0
	physics_body.linear_velocity.y = 0
	physics_body.linear_velocity.z = 0


func destroy_wind():
	physics_body.mode = RigidBody.MODE_RIGID

	physics_body.linear_velocity.x = 25 + rng.randf() * 5
	physics_body.linear_velocity.y = rng.randf() * 1


func destroy_fire():
	var module_material = __clone_material()
	module_material.albedo_color.r = rng.randf_range(0.75, 0.85)
	module_material.albedo_color.g = 0.7
	module_material.albedo_color.b = 0.7

	var safe_half_grid_size = DATA.GRID_SIZE * 0.4

	for iy in module_size.y:
		for ix in module_size.x - 1:
			for __ in rng.randi_range(1, 2):
				for ixdir in [-1, 1]:
					var fire = fire_scenes[rng.randi_range(0, fire_scenes.size() - 1)].instance()
					physics_body.add_child(fire)
					fire.set_translation(Vector3(
						DATA.GRID_SIZE * ix * ixdir + rng.randf_range(
							-1 * safe_half_grid_size, safe_half_grid_size
						),
						DATA.GRID_SIZE * iy + 0.1,
						rng.randf_range(
							-1 * safe_half_grid_size, safe_half_grid_size
						)
					))


func destroy_temperature():
	var module_material = __clone_material()
	module_material.roughness = rng.randf_range(0.0, 0.15)
	module_material.emission_enabled = true

	var base = rng.randf_range(0.15, 0.35)
	var b = rng.randf_range(0.1, 0.4)
	module_material.emission.r = base + rng.randf_range(0.0, 0.15)
	module_material.emission.g = base + b * 0.7
	module_material.emission.b = base + b

	var albedo = rng.randf_range(0.7, 0.85)
	module_material.albedo_color.r = albedo
	module_material.albedo_color.g = albedo
	module_material.albedo_color.b = albedo


func __clone_material():
	var module_model = physics_body.get_child(2).get_child(0)
	var module_material = module_model.get_active_material(0).duplicate()
	module_model.set_surface_material(0, module_material)
	return module_material
