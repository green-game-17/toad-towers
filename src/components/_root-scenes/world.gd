extends Node


func _ready():
	EVENTS.connect('stop_music', self, '_on_stop_music')


func _on_stop_music():
	$BackgroundMusic.stop()
