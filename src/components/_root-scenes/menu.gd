extends Control


const FILE_PATH_SAVE = "user://savegame.txt"

var page = 0


func _ready():
	if STATE.get_show_level_select():
		__save_data()
		_on_start()
		STATE.set_show_level_select(false)
	
	__load_data()
	$MainMenu/StartButton.connect('pressed', self, '_on_start')
	$MainMenu/OptionsButton.connect('pressed', self, '_on_options')
	$LevelSelect/BackButton.connect('pressed', self, '_on_back')
	$Options/VBox/DeleteButton.connect('pressed', self, '_on_delete_data')
	$Options/BackButton.connect('pressed', self, '_on_back')
	$LevelSelect/NextLevels.connect('pressed', self, '_on_next_levels')
	$LevelSelect/PreviousLevels.connect('pressed', self, '_on_previous_levels')
	for child in $LevelSelect/Grid.get_children():
		child.connect('pressed', self, '_on_level_selected', [int(child.get_text()) - 1])
	for child in $LevelSelect/Grid2.get_children():
		child.connect('pressed', self, '_on_level_selected', [int(child.get_text()) - 1])
	$Options/VBox/Volume/Slider.connect('value_changed', self, '_on_volume_changed')
	
	__draw_levels()

	randomize()
	var label = $MainMenu/CreditContainer/GameLabel
	var names = [
		'toxs1ck',
		'Momo_Hunter',
		'LauraWilkinson',
		'LucaVazz',
	]
	names.shuffle()
	label.set_text('Game by: %s, %s, %s and %s'%names)


func __draw_levels():
	if DATA.CONTRACTS.size() > 8:
		$LevelSelect/NextLevels.show()
	else:
		for i in $LevelSelect/Grid.get_child_count():
			var level_number = i + 1
			var child = $LevelSelect/Grid.get_child(i)
			if level_number > DATA.CONTRACTS.size():
				child.hide()
		for i in $LevelSelect/Grid2.get_child_count():
			var level_number = i + 5
			var child = $LevelSelect/Grid2.get_child(i)
			if level_number > DATA.CONTRACTS.size():
				child.hide()
	
	for level_number in $LevelSelect/Grid.get_child_count():
		var child = $LevelSelect/Grid.get_child(level_number)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number))
		
		__check_unlock_level(child)
		
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))
	
	for i in $LevelSelect/Grid2.get_child_count():
		var level_number = i + 4
		var child = $LevelSelect/Grid2.get_child(i)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number))
		
		__check_unlock_level(child)
		
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))


func _on_start():
	$MainMenu.hide()
	$LevelSelect.show()


func _on_options():
	$MainMenu.hide()
	$Options.show()


func _on_back():
	$LevelSelect.hide()
	$Options.hide()
	$MainMenu.show()
	while page > 0:
		_on_previous_levels()
	__save_data()


func _on_next_levels():
	page += 1
	
	for i in $LevelSelect/Grid.get_child_count():
		var level_number = i + 1 + (page * 8)
		var child = $LevelSelect/Grid.get_child(i)
		if level_number > DATA.CONTRACTS.size():
			child.hide()
		else:
			child.show()
		child.set_text(str(level_number))
		__check_unlock_level(child)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number - 1))
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))
	
	for i in $LevelSelect/Grid2.get_child_count():
		var level_number = i + 5 + (page * 8)
		var child = $LevelSelect/Grid2.get_child(i)
		if level_number > DATA.CONTRACTS.size():
			child.hide()
		else:
			child.show()
		child.set_text(str(level_number))
		__check_unlock_level(child)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number - 1))
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))
	
	if ceil(DATA.CONTRACTS.size() / 8.0) == page + 1:
		$LevelSelect/NextLevels.hide()
	if page > 0:
		$LevelSelect/PreviousLevels.show()


func _on_previous_levels():
	page -= 1
	
	for i in $LevelSelect/Grid.get_child_count():
		var level_number = i + 1 + (page * 8)
		var child = $LevelSelect/Grid.get_child(i)
		if level_number > DATA.CONTRACTS.size():
			child.hide()
		else:
			child.show()
		child.set_text(str(level_number))
		__check_unlock_level(child)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number - 1))
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))
	
	for i in $LevelSelect/Grid2.get_child_count():
		var level_number = i + 5 + (page * 8)
		var child = $LevelSelect/Grid2.get_child(i)
		if level_number > DATA.CONTRACTS.size():
			child.hide()
		else:
			child.show()
		child.set_text(str(level_number))
		__check_unlock_level(child)
		var lilypads = child.get_node('HBox')
		var level_result = STATE.get_level_result(str(level_number - 1))
		for j in lilypads.get_child_count():
			if j < level_result:
				lilypads.get_child(j).set_self_modulate(Color(1, 1, 1, 1))
			else:
				lilypads.get_child(j).set_self_modulate(Color(0, 0, 0, 1))
	
	if page == 0:
		$LevelSelect/PreviousLevels.hide()
	if floor(DATA.CONTRACTS.size() / 8.0) > page:
		$LevelSelect/NextLevels.show()


func _on_volume_changed(new_value: float):
	STATE.set_volume(new_value)
	$Options/VBox/Volume/Value.set_text(str(new_value))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), new_value - 80)


func _on_level_selected(child_number):
	var level_number = 1
	if child_number < 4:
		level_number = int($LevelSelect/Grid.get_child(child_number).get_text())
	else:
		level_number = int($LevelSelect/Grid2.get_child(child_number - 4).get_text())
	if not STATE.is_level_unlocked(level_number):
		return
	
	STATE.set_level(level_number - 1)
	$Music.stop()
	SCENE.goto_scene('world')


func __save_data():
	var file = File.new()
	file.open(FILE_PATH_SAVE, File.WRITE)
	file.store_line(to_json({
		'volume': STATE.get_volume(),
		'levels': STATE.get_level_results(),
		'unlocked_levels': STATE.get_unlocked_levels()
	}))
	file.close()


func __load_data():
	var file = File.new()
	if file.file_exists(FILE_PATH_SAVE):
		file.open(FILE_PATH_SAVE, File.READ)
		var data = parse_json(file.get_line())
		if data is float:
			__save_data()
			STATE.set_volume(data)
		else:
			STATE.set_volume(data.volume)
			for key in data.levels:
				STATE.add_level_result(key, data.levels[key])
			if 'unlocked_levels' in data:
				for i in data.unlocked_levels:
					STATE.unlock_level(i)
		$Options/VBox/Volume/Value.set_text(str(STATE.get_volume()))
		$Options/VBox/Volume/Slider.set_value(STATE.get_volume())
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), STATE.get_volume() - 80)
		file.close()


func _process(_delta):
	if $FrogTextureRect.rect_size.y > 10000:
		$FrogTextureRect.rect_size.y = 100

	$FrogTextureRect.rect_size.y *= 1.0003
	if $FrogTextureRect.rect_size.y > 300:
		$FrogTextureRect.rect_size.y *= 1.1


func __check_unlock_level(child):
	var level_number = int(child.get_text())
	if STATE.is_level_unlocked(level_number):
		child.set_self_modulate(Color(1, 1, 1, 1))
		child.set_disabled(false)
	else:
		child.set_self_modulate(Color(0.3, 0.3, 0.3, 1))
		child.set_disabled(true)


func _on_delete_data():
	var file = File.new()
	if file.file_exists(FILE_PATH_SAVE):
		var directory = Directory.new()
		directory.remove(FILE_PATH_SAVE)
		STATE.set_volume(80)
		$Options/VBox/Volume/Value.set_text(str(80))
		$Options/VBox/Volume/Slider.set_value(80)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), 0)
		STATE.reset_level_results()
		STATE.reset_unlocked_levels()
		__draw_levels()
