extends Node


const SCORE_PRECISION_MULTIPLIER = 100
const COSTS_FOR_DELETE = 1000


# debug
var is_debug = false


# Mutable, shared state data:
var __level_module_blocks = []
var __level_environment = ID.GRASS
var __level_width = 10
var __selected_level = 0
var __budget = 0
var __discarded_cards = 0
var __show_level_select = false

var __level_results = {}
var __unlocked_levels = [1]

var __delete_active = false

var __volume = 80


func _init():
	reset_all()


func reset_all():
	reset_level(0, ID.GRASS, 10)


func reset_level(selected, environment, width):
	__level_module_blocks = [[]]
	__level_module_blocks[0].resize(width + 1)

	__selected_level = selected
	__level_environment = environment
	__level_width = width
	__discarded_cards = 0


func set_level(selected):
	var level_data = DATA.get_level_data(selected)
	reset_level(selected, level_data.area, level_data.build_area_size)
	set_budget(level_data.budget)


func get_level() -> int:
	return __selected_level


func set_show_level_select(show_it: bool):
	__show_level_select = show_it


func get_show_level_select() -> bool:
	return __show_level_select


func is_level_unlocked(level_number: int) -> bool:
	return level_number in __unlocked_levels


func unlock_level(level_number: int):
	if not level_number in __unlocked_levels:
		__unlocked_levels.append(level_number)


func get_unlocked_levels() -> Array:
	return __unlocked_levels


func reset_unlocked_levels():
	__unlocked_levels = [1]


func inc_discarded_cards():
	__discarded_cards += 1


func get_discarded_cards() -> int:
	return __discarded_cards


func add_module(id, x, y, size) -> void:
	assert(x >= 0 and y >= 0, '[STATE]: add_module: x or y invalid, x=%d, y=%d'%[x, y])

	if __level_module_blocks.size() < y + size.y:
		var old_size = __level_module_blocks.size()
		__level_module_blocks.resize(y + size.y)
		for iy in range(old_size, y + size.y):
			__level_module_blocks[iy] = []
			__level_module_blocks[iy].resize(__level_width + 1)

	for iy in range(y, y + size.y):
		for ix in range(x, x + size.x):
			__level_module_blocks[iy][ix] = id + str(iy - y) + str(ix - x)

	if is_debug:
		print('new tower:')
		for iy in __level_module_blocks.size():
			var iiy = __level_module_blocks.size() - iy - 1
			var row = []
			for ix in __level_module_blocks[iiy].size():
				row.append('.' if (__level_module_blocks[iiy][ix]) == null else '#')
			print('  %d: %s'%[iiy, row])


func remove_module(x, y, size):
	for iy in range(y, y + size.y):
		for ix in range(x, x + size.x):
			__level_module_blocks[iy][ix] = null

	if is_debug:
		print('new tower:')
		for iy in __level_module_blocks.size():
			var iiy = __level_module_blocks.size() - iy - 1
			var row = []
			for ix in __level_module_blocks[iiy].size():
				row.append('.' if (__level_module_blocks[iiy][ix]) == null else '#')
			print('  %d: %s'%[iiy, row])


func get_module_is_colliding(x, y, size) -> bool:
	if __level_module_blocks.size() < y:
		return false

	for iy in range(y, min(__level_module_blocks.size(), y + size.y)):
		for ix in range(x, x + size.x):
			if __level_module_blocks[iy][ix] != null:
				return true

	return false


func can_delete_module(module) -> bool:
	var mod_x = module.tower_position.x
	var mod_y = module.tower_position.y
	var mod_size = module.module_size
	
	var positive_pos = []
	
	if __level_module_blocks.size() > mod_y + mod_size.y:
		var check_y = mod_y + mod_size.y
		for i in range(mod_x, mod_x + mod_size.x):
			if __level_module_blocks[check_y][i] != null:
				var found_module = __level_module_blocks[check_y][i]
				var index = int(found_module[found_module.length() - 1])
				var id = found_module.left(found_module.length() - 2)
				for j in range(DATA.CARDS[id].size.x):
					var x_pos = i - index + j
					if (x_pos < mod_x and __level_module_blocks[check_y - 1][x_pos] != null) or (
						x_pos >= mod_x + mod_size.x and __level_module_blocks[check_y - 1][x_pos] != null):
						positive_pos.append(i)
						break
			else:
				positive_pos.append(i)
		for check_x in range(mod_x, mod_x + mod_size.x):
			if !check_x in positive_pos:
				return false
	return true


func get_module_is_supported(x, y, size) -> bool:
	if y == 0:
		return true
	
	if y > __level_module_blocks.size():
		return false

	for ix in range(x, x + size.x):
		var id = __level_module_blocks[y - 1][ix]
		if id != null and not id.left(id.length() - 2) in DATA.get_card_ids_by_category(ID.ROOFING):
			return true

	return false


func get_level_environment() -> String:
	return __level_environment


func get_level_width() -> int:
	return __level_width


func get_tower_height() -> int:
	for iy in __level_module_blocks.size():
		var iiy = __level_module_blocks.size() - iy - 1
		for block in __level_module_blocks[iiy]:
			if block != null:
				return iiy + 1
		
	return 0


func get_selected_level() -> int:
	return __selected_level


func set_budget(value: int):
	__budget = value


func decrease_budget(value: int):
	__budget -= value


func get_budget() -> int:
	return __budget


func has_enough_budget(id: String) -> bool:
	return DATA.CARDS[id].costs <= __budget


func has_enough_budget_to_delete() -> bool:
	return COSTS_FOR_DELETE <= __budget


func decrease_budget_by_delete():
	__budget -= COSTS_FOR_DELETE


func set_delete_active(is_active: bool):
	__delete_active = is_active


func get_delete_active() -> bool:
	return __delete_active


func add_level_result(level_number: String, lilypads: int):
	if level_number in __level_results:
		if __level_results[level_number] < lilypads:
			__level_results[level_number] = lilypads
	else:
		__level_results[level_number] = lilypads


func get_level_result(level_number: String) -> int:
	if level_number in __level_results:
		return __level_results[level_number]
	return 0


func get_level_results() -> Dictionary:
	return __level_results.duplicate(true)


func reset_level_results():
	__level_results = {}


func format_number(count: int) -> String:
	if count >= 1_000_000_000:
		return 'way too much'
	elif count >= 1_000_000:
		return '%s %03d %03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		return '%s %03d' % [floor(count / 1_000), count % 1_000]
	else:
		return str(count)


func format_disaster_value(disaster_id: String, value: float) -> String:
	match disaster_id:
		ID.EARTHQUAKE, ID.EARTHQUAKE_RESISTANCE:
			return 'Magnitude %.*f'%[2 if int(value) % 10 != 0 else 1, value / 100]
		ID.TORNADO, ID.SNAKENADO, ID.WIND_RESISTANCE, ID.SNAKENADO_RESISTANCE:
			return 'F%.*f'%[2 if int(value) % 10 != 0 else 1, value / 100]
		ID.FLOOD, ID.WATER_RESISTANCE:
			return str(value / 10) + 'm'
		ID.COLDNESS, ID.TEMPERATURE_RESISTANCE:
			return str(value / -10) + '°C'
		ID.INFERNO, ID.FIRE_RESISTANCE:
			match value / 100:
				1.0:
					return str((value / 100) as int) + 'st circle of hell, Limbo'
				2.0:
					return str((value / 100) as int) + 'nd circle of hell, Lust'
				3.0:
					return str((value / 100) as int) + 'rd circle of hell, Gluttony'
				4.0:
					return str((value / 100) as int) + 'th circle of hell, Greed'
				5.0:
					return str((value / 100) as int) + 'th circle of hell, Wrath'
				6.0:
					return str((value / 100) as int) + 'th circle of hell, Heresy'
				7.0:
					return str((value / 100) as int) + 'th circle of hell, Violence'
				8.0:
					return str((value / 100) as int) + 'th circle of hell, Fraud'
				9.0:
					return str((value / 100) as int) + 'th circle of hell, Treachery'
			return str(value / 100) + 'th circle of hell'
	return str(value)


func calc_stability_score() -> int:
	var count_bad = 0
	var occupied_spots = 0
	
	for i in __level_module_blocks[0]:
		if i != null:
			occupied_spots += 1
	
	var stories_per_reduction = (2 * (occupied_spots / __level_width)) + 1
	var score = 4 - (floor(get_tower_height() / stories_per_reduction) * 0.5)

	for iy in range(1, __level_module_blocks.size()):
		for ix in range(0, __level_width + 1):
			if __level_module_blocks[iy][ix] != null:
				var id = __level_module_blocks[iy - 1][ix]
				if id == null or id.left(id.length() - 2) in DATA.get_card_ids_by_category(ID.ROOFING):
					count_bad += 1
	
	score -= count_bad * 0.2
	
	return to_score(max(score, 0))


func calc_module_category_share_score(category_id: String) -> int:
	var count = get_module_category_count(category_id)
	var total = get_total_blocks_count()
	if count == 0:
		return 0
	return to_score(count / total)


func calc_specific_modules_share_score(ids: Array) -> int:
	var count = get_multiple_modules_count(ids)
	var total = get_total_blocks_count()
	if count == 0:
		return 0
	return to_score(count / total)


func calc_happiness_score() -> int:
	var entertainment_value = get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR) * 10
	var living_value = get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
	if entertainment_value == 0 or living_value == 0:
		return 0
	return to_score(entertainment_value / living_value)


func calc_drowning_is_safe(safe_height: int) -> bool:
	for iy in range(0, min(safe_height, __level_module_blocks.size())):
		for block in __level_module_blocks[iy]:
			if block.left(block.length() - 2) in DATA.get_card_ids_by_category(ID.LIVING_SPACE):
				return false
	return true
	


func get_module_category_count(category_id: String) -> float:
	var module_ids = DATA.get_card_ids_by_category(category_id)
	return get_multiple_modules_count(module_ids)
	

func get_module_category_value(category_id: String, stat_id: String) -> float:
	var total_value = 0.0
	for id in DATA.get_card_ids_by_category(category_id):
		var type_value = DATA.get_building_data(id).stats.get(stat_id, null)
		if type_value != null:
			total_value += get_single_module_count(id) * type_value
	return total_value


func get_multiple_modules_count(ids: Array) -> float:
	var count = 0.0
	for id in ids:
		count += get_single_module_count(id)
	return count


func get_single_module_count(id: String) -> float:
	var count = get_blocks_count(id)
	var module_data = DATA.get_building_data(id)
	var module_size = module_data.size.x * module_data.size.y
	if count == 0:
		return 0.0
	return count / module_size


func get_blocks_count(id: String) -> float:
	var count = 0.0

	for row in __level_module_blocks:
		for block in row:
			if block and block.begins_with(id):
				count += 1

	return count


func get_total_blocks_count() -> float:
	var count = 0.0

	for row in __level_module_blocks:
		for block in row:
			if block != null:
				count += 1

	return count


func to_score(score: float) -> int:
	if score < 0:
		score = 0

	return int(score * SCORE_PRECISION_MULTIPLIER)


func set_volume(value: float):
	__volume = value


func get_volume() -> float:
	return __volume
