extends Node2D


var debugSignals = true


# Signals:
signal special_card_visible(is_visible)
signal module_selected(id)
signal module_deselected()
signal module_placed()
signal destroy_quake()
signal destroy_wind()
signal destroy_snakenado()
signal destroy_temperature()
signal destroy_fire()
signal destroy_water()
signal show_pause_menu()
signal ready_to_submit()
signal last_disaster_success()
signal level_success()
signal disaster_struck(disaster_id)
signal level_failed(disaster_id)
signal delete_triggered(is_active)
signal stop_music()


func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			elif s.args.size() == 3:
				connect(s.name, self, "__on_signal_three_args", [s.name])
			else:
				push_error('[EventManager]! %s name has more than 2 args, debug not set up!' % s.name)


func __on_signal_no_args(name):
	print('[EventManager]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[EventManager]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[EventManager]: got signal %s with args %s , %s' % [name, arg1, arg2])


func __on_signal_three_args(arg1, arg2, arg3, name):
	print('[EventManager]: got signal %s with args %s , %s , %s' % [name, arg1, arg2, arg3])
